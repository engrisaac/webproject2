from django import forms


class PortFolio(forms.Form):
     name = forms.CharField(max_length = 20,
      widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder' : 'Your Name' }))
     email = forms.EmailField(widget = forms.TextInput(attrs ={'class': 'form-control' , 'placeholder' : 'Your Email' }))
     subject = forms.CharField(widget = forms.TextInput(attrs={ 'class': 'form-control' , 'placeholder' : 'Subject'}))
     message = forms.CharField(widget = forms.Textarea(attrs={ 'class' : 'form-control' , 'placeholder': 'Message'}))