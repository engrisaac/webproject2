from django.shortcuts import render
from .forms import PortFolio
from .models import Portfolio

def index(request):

    if request.method == "POST":
        form = PortFolio(request.POST)

        if form.is_valid():
            new_form = Portfolio( name = request.POST['name'], email = request.POST['email'] ,
            subject = request.POST['subject'] , message = request.POST['message'])
            new_form.save()
    else:

        form = PortFolio()


    context = { "form" : form }


    return render(request, 'webapp2/index.html' , context)




def single(request):




    return render(request, 'webapp2/single.html')